#!/bin/bash

#########################################################################
#  This code is written by Athul R.T[athul@disroot.org]                 # 
#                                                                       #
#                                                                   	#
#                                                                       #
#       =============== Instructions===================                 #
#   This bash program is created to update def-bas, def-ini and betac   #
#  from a file to DIRHB FORTRAN code. Please make sure that the spacing #
#    after def-bas, def-ini, betac should be maintained as before.      #
#  											    						#
#																		#
#	If it didn't run, give execute permission by entering				#
#	`sudo chmod +x test3script.sh`.										#
#																		#
#	How to Run:															#
#	Copy this file along with csv file									#
# to the folder containing `dirhbz.f` file. 							#
#	Open terminal in same folder.										#
#	run `./test3script.sh in terminal.									#
#                                                                       #
#########################################################################


mkdir new_test
sed -n 'p;n' < po.csv > po_even.csv
echo "$(tail -n +2 po_even.csv)" > po_even.csv
counter=($(cat po_even.csv | awk '{ print $3 }'))
counter2=($(cat po_even.csv | awk '{ print $2 }'))
for index in ${!counter[*]}; do 

  echo "${counter[$index]} ===== ${counter2[$index]}"
sed -i 's/Kr .*$/Kr '${counter2[$index]}'/ ; s/def-bas  = .*$/def-bas  = '${counter[$index]}'/ ;  s/def-ini  = .*$/def-ini  = '${counter[$index]}'/ ; s/betac    = .*$/betac    = '${counter[$index]}'/' dirhb.dat
./run
mv -- dirhb.out new_test/$index;
done

