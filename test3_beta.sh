#!/bin/bash
#########################################################################
#  This code is written by Athul R.T[athul@disroot.org]                 # 
#                                                                       #
#                                                                      	#
#                                                                       #
#       =============== Instructions===================                 #
#   This bash program is created to update beta values from a           #
#   range to DIRHB FORTRAN code. Please make sure that the spacing      #
#    after def-bas, def-ini, betac should be maintained as before.      #
#   `seq -0.6 0.01 0.6` => start, step, stop     						#
#																		#
#	If it didn't run, give execute permission by entering				#
#	`sudo chmod +x test3_beta.sh`.										#
#																		#
#	How to Run:															#
#	Copy this file to the folder containing `dirhbz.f` file. 			#
#	Open terminal in same folder.										#
#	run `./test3_beta.sh in terminal.									#
#                                                                       #
#########################################################################

mkdir new_beta

for index in `seq -0.6 0.01 0.6`; do 
sed -i 's/def-bas  = .*$/def-bas  = '$index'/ ;  s/def-ini  = .*$/def-ini  = '$index'/ ; s/betac    = .*$/betac    = '$index'/' dirhb.dat
./run
mv -- dirhb.out new_beta/$index;
done

