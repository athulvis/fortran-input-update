# FORTRAN Input Update

This is a shell script written to update input values for a particular FORTRAN code

## __Instructions__

## test3script.sh

  This bash program is created to update def-bas, def-ini and betac  from a file to DIRHB FORTRAN code. Please make sure that the spacing after def-bas, def-ini, betac should be maintained as before. If it didn't run, give execute permission by entering	
			
`sudo chmod +x test3script.sh`									
																		
###	How to Run:															
* Copy this file along with csv file to the folder containing `dirhbz.f` file. 							
* Open terminal in same folder.										
* run `./test3script.sh` in terminal.	

   ## test3beta.sh          
   This bash program is created to update beta values from a range to DIRHB FORTRAN code. Please make sure that the spacing after def-bas, def-ini, betac should be maintained as before.  
    
   `seq -0.6 0.01 0.6` => start, step, stop     						
																	
	If it didn't run, give execute permission by entering		
		
`sudo chmod +x test3_beta.sh`.										
																	
###	How to Run:															
* Copy this file to the folder containing `dirhbz.f` file. 			
* Open terminal in same folder.										
* run `./test3_beta.sh in terminal.	
